import {create_edit_button} from "../actions.component/edit.component/edit.component.js";
import {create_delete_button} from "../actions.component/delete.component/delete.component.js";


export function add(item){
    if (Storage != undefined){
        if (localStorage.getItem("CRUD_ACADEMLO") == undefined){
            let items = [item]
            localStorage.setItem("CRUD_ACADEMLO",JSON.stringify(items));
        }
        else{  
            let bitem = JSON.parse(localStorage.getItem("CRUD_ACADEMLO"))
            bitem.push(item)
            localStorage.setItem("CRUD_ACADEMLO",JSON.stringify(bitem))
        }

    }else{
        alert("Este navegador no soporta local storage y no podemos guardar o recuperar los datos para proximas sesiones")
    }
}

export function borrar(index){
    if (Storage != undefined){
        if (localStorage.getItem("CRUD_ACADEMLO") == undefined){
            alert("No hay nada que borrar del storage")
        }
        else{  
            let bitem = JSON.parse(localStorage.getItem("CRUD_ACADEMLO"))
            let newitem = bitem.filter(function(item){
                if (item.index == index){
                    return false
                } else{
                    return true
                }

            })
            localStorage.setItem("CRUD_ACADEMLO",JSON.stringify(newitem))
        }

    }else{
        alert("Este navegador no soporta local storage y no podemos guardar o recuperar los datos para proximas sesiones")
    }
}

export function update(index,info){
    if (Storage != undefined){
        if (localStorage.getItem("CRUD_ACADEMLO") == undefined){
            alert("No hay nada que actualizar del storage")
        }
        else{  
            let bitem = JSON.parse(localStorage.getItem("CRUD_ACADEMLO"))
            let eitems = bitem.filter(function(item){
                if (item.index == index){
                    return false
                } else{
                    return true
                }

            })
            let target = bitem.filter(function(item){
                if (item.index == index){
                    return true
                } else{
                    return false
                }
            })[0]

            target.fullname = info.text_fullname
            target.propietario = info.text_propietario

            eitems.push(target)

            localStorage.setItem("CRUD_ACADEMLO",JSON.stringify(eitems))
        }

    }else{
        alert("Este navegador no soporta local storage y no podemos guardar o recuperar los datos para proximas sesiones")
    }

}


function create_table(item){

    let num_registro_ = item.index
    let fullname_ = item.fullname
    let propietario_ = item.propietario


    let table_content = document.getElementById("table-content")
    let row = document.createElement("div")
    row["index"] = num_registro_
    let col1 = document.createElement("div")
    let col2 = document.createElement("div")
    let col3 = document.createElement("div")
    let actions = document.createElement("div")
    let fullname = document.createElement("h2")
    let propietario = document.createElement("h2")

    row.className = "row"
    col1.className = "col-4"
    col2.className = "col-4"
    col3.className = "col-4"
    actions.className = "actions"

    fullname.innerText = fullname_
    propietario.innerText = propietario_

    actions.appendChild(create_edit_button(num_registro_))
    actions.appendChild(create_delete_button(num_registro_))

    col1.appendChild(fullname)
    col2.appendChild(propietario)
    col3.appendChild(actions)

    row.appendChild(col1)
    row.appendChild(col2)
    row.appendChild(col3)

    table_content.appendChild(row)


}


export function gchekpoint(){

    if (Storage != undefined){
        if (localStorage.getItem("CRUD_ACADEMLO") == undefined){
            return null
        }
        else{  
            let bitems = JSON.parse(localStorage.getItem("CRUD_ACADEMLO"))
            bitems.map(create_table)
        }

    }else{
        alert("Este navegador no soporta local storage y no podemos guardar ni recuperar información de sesiones anteriores")
    }

}