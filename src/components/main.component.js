import * as saveUser from "./actions.component/save-user.component/save-user.component.js";
import { gchekpoint } from "./localStorage.component/localStorage.component.js";

function __init__() {
  $(function () {
    $(".input").load("./src/components/input.component/input.component.html");
    $(".save-user").load(
      "./src/components/actions.component/save-user.component/save-user.component.html"
    );
    $(".content").load(
      "./src/components/content.component/content.component.html"
    );
  });
}
setTimeout(function () {
  saveUser.init();
  gchekpoint();
}, 100);

export var init = __init__();
