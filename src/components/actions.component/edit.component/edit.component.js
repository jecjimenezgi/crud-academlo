import { range,testinput } from "../../tools.component/tools.component.js";
import {create_delete_button} from "../delete.component/delete.component.js";
import * as db from "../../localStorage.component/localStorage.component.js"


function actualizar(e){
    let table_content = document.getElementById("table-content");
    let rows = table_content.getElementsByClassName("row");
    let index = range(0, rows.length).filter(function (i) {
      if (rows[i].index == e.target.index) {
        return true;
      } else {
        return false;
      }
    })[0];
    let fullname = rows[index].getElementsByClassName("col-4")[0]
    let propietario = rows[index].getElementsByClassName("col-4")[1]
    let text_fullname = fullname.getElementsByClassName("input")[0].value
    let text_propietario = propietario.getElementsByClassName("input")[0].value



    if (!text_fullname) {
      fullname.getElementsByClassName("input")[0].style["background-color"] = "rgba(255, 0, 0,0.5)";
    } else {
      fullname.getElementsByClassName("input")[0].style["background-color"] = "white";
    }

    if (!text_propietario) {
      propietario.getElementsByClassName("input")[0].style["background-color"] = "rgba(255, 0, 0,0.5)";
    } else {
      propietario.getElementsByClassName("input")[0].style["background-color"] = "white";
    }

    if (!text_fullname || !text_propietario) {
      setTimeout(function() {
        alert("!No se pudieron actualizar los datos del vehiculo!")
      }, 100);
      return null;
    }

    db.update(e.target.index,{
      "text_fullname":text_fullname,
      "text_propietario": text_propietario
    })



    let xfullname = rows[index].getElementsByClassName("col-4")[0]
    let xpropietario = rows[index].getElementsByClassName("col-4")[1]
    let xdactions = rows[index].getElementsByClassName("col-4")[2]
    xdactions.remove()
    xfullname.remove();
    xpropietario.remove();

    let col1 = document.createElement("div")
    let col2 = document.createElement("div")
    let col3 = document.createElement("div")
    let zactions = document.createElement("div")
    let zfullname = document.createElement("h2")
    let zpropietario = document.createElement("h2")

    col1.className = "col-4"
    col2.className = "col-4"
    col3.className = "col-4"
    zactions.className = "actions"

    zfullname.innerText = text_fullname
    zpropietario.innerText = text_propietario

    col1.appendChild(zfullname)
    col2.appendChild(zpropietario)

    rows[index].appendChild(col1)
    rows[index].appendChild(col2)

    zactions.appendChild(create_edit_button(e.target.index))
    zactions.appendChild(create_delete_button(e.target.index))

    col3.appendChild(zactions)
    rows[index].appendChild(col3)

}

function edit(e) {
  let table_content = document.getElementById("table-content");
  let rows = table_content.getElementsByClassName("row");
  let index = range(0, rows.length).filter(function (i) {
    if (rows[i].index == e.target.index) {
      return true;
    } else {
      return false;
    }
  })[0];
  let fullname = rows[index].getElementsByClassName("col-4")[0]
  let propietario = rows[index].getElementsByClassName("col-4")[1]
  let dactions = rows[index].getElementsByClassName("col-4")[2]
  dactions.remove()
  fullname.remove();
  propietario.remove();
  let col1 = document.createElement("div");
  col1.className = "col-4";
  let col2 = document.createElement("div");
  col2.className = "col-4";
  let fullname_input = document.createElement("input");
  let propietario_input = document.createElement("input");
  fullname_input.className = "input";
  propietario_input.className = "input";
  fullname_input.type = "text";
  propietario_input.type = "text";
  fullname_input.placeholder = "Ingrese Matricula-Modelo";
  propietario_input.placeholder = "Ingrese el propietario";
  col1.appendChild(fullname_input);
  col2.appendChild(propietario_input);
  rows[index].appendChild(col1)
  rows[index].appendChild(col2)

  let col3 = document.createElement("div")
  col3.className = "col-4"
  let actions = document.createElement("div");
  actions.className = "actions";
  let update = document.createElement("div");
  update.className = "update";
  let text = document.createElement("h2");
  text.innerText = "Actualizar";
  text["index"] = e.target.index;
  update.appendChild(text);
  update["index"] = e.target.index;
  update.addEventListener("click",actualizar)
  actions.appendChild(update);
  col3.appendChild(actions)
  rows[index].appendChild(col3)
}

export function create_edit_button(index) {
  let button = document.createElement("div");
  let text = document.createElement("h2");
  button.className = "editar";
  text.innerText = "Editar";
  text.index = index;
  button.appendChild(text);
  button["index"] = index;
  button.addEventListener("click", edit);
  return button;
}
