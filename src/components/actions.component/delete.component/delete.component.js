import {range} from "../../tools.component/tools.component.js";
import * as db from "../../localStorage.component/localStorage.component.js"


function borrar(e) {
  let table_content = document.getElementById("table-content");
  let rows = table_content.getElementsByClassName("row");
  let index = range(0,rows.length).filter(function(i){
      if( rows[i].index == e.target.index){
          return true
      }
      else{
          return false
      }
  })[0]
  db.borrar(e.target.index)
  rows[index].remove()
}

export function create_delete_button(index) {
  let button = document.createElement("div");
  let text = document.createElement("h2");
  button.className = "borrar";
  text.innerText = "Borrar";
  text.index = index;
  button.appendChild(text);
  button["index"] = index;
  button.addEventListener("click", borrar);
  return button;
}
