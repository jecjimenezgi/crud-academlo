import * as input from "../../input.component/input.component.js";
import {create_edit_button} from "../edit.component/edit.component.js";
import {create_delete_button} from "../delete.component/delete.component.js";
import * as db from "../../localStorage.component/localStorage.component.js"



function save_user(){

    try{
        let num_registro = new Date().valueOf();

        let fields = input.get_fields()

        db.add({ 
            "index": num_registro,
            "fullname": fields["first_name"] + "-" + fields["last_name"],
            "propietario": fields["propietario"]
        })
    
        let table_content = document.getElementById("table-content")
        let row = document.createElement("div")
        row["index"] = num_registro
        let col1 = document.createElement("div")
        let col2 = document.createElement("div")
        let col3 = document.createElement("div")
        let actions = document.createElement("div")
        let fullname = document.createElement("h2")
        let propietario = document.createElement("h2")
    
        row.className = "row"
        col1.className = "col-4"
        col2.className = "col-4"
        col3.className = "col-4"
        actions.className = "actions"
    
        fullname.innerText = fields["first_name"] + "-" +  fields["last_name"]
        propietario.innerText = fields["propietario"]
    
        actions.appendChild(create_edit_button(num_registro))
        actions.appendChild(create_delete_button(num_registro))
    
        col1.appendChild(fullname)
        col2.appendChild(propietario)
        col3.appendChild(actions)
    
        row.appendChild(col1)
        row.appendChild(col2)
        row.appendChild(col3)
    
        table_content.appendChild(row)

    }catch(e){
        setTimeout(function() {
            alert("¡No es posible resgitar el vehiculo!")
          }, 100);
    }
}

function __init__(){
    var button_save = document.getElementById("button-save")
    button_save.addEventListener("click", save_user)
}

export var init = __init__



