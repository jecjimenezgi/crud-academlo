import {testinput} from "../tools.component/tools.component.js"

export function get_fields() {
  let first_name = document.getElementById("firstname-input");
  let last_name = document.getElementById("lastname-input");
  let propietario = document.getElementById("propietario-input");

  if (!first_name.value) {
    first_name.style["background-color"] = "rgba(255, 0, 0,0.5)";
  } else {
    first_name.style["background-color"] = "white";
  }

  if (!last_name.value) {
    last_name.style["background-color"] = "rgba(255, 0, 0,0.5)";
  } else {
    last_name.style["background-color"] = "white";
  }

  if (!propietario.value) {
    propietario.style["background-color"] = "rgba(255, 0, 0,0.5)";
  } else {
    propietario.style["background-color"] = "white";
  }

  if (!first_name.value || !last_name.value || !propietario.value ) {
    return null;
  }

  let fields = {
    first_name: first_name.value,
    last_name: last_name.value,
    propietario: propietario.value,
  };
  first_name.value = "";
  last_name.value = "";
  propietario.value = "";

  return fields;
}

function __init__() {}

export var init = __init__();
